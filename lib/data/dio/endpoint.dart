class ApiConstant {
  ApiConstant._();

  static const String GET_APP_VERSION = '/api/v1/mobile/get-version-current';
}
