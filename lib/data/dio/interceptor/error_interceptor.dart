import 'package:dio/dio.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';

class ErrorInterceptor extends Interceptor {
  ErrorInterceptor();

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    switch (err.type) {
      case DioExceptionType.connectionTimeout:
      case DioExceptionType.sendTimeout:
      case DioExceptionType.receiveTimeout:
        throw TimeoutException(requestOptions: err.requestOptions);
      case DioExceptionType.badResponse:
        switch (err.response?.statusCode) {
          case 401:
          //todo handle logout
          case 400:
          case 404:
          case 409:
          case 500:
            throw UnknownException(
              requestOptions: err.requestOptions,
            );
          case 502:
            throw ServiceUnavailableException(
              requestOptions: err.requestOptions,
            );
        }
        break;
      case DioExceptionType.cancel:
        break;
      case DioExceptionType.badCertificate:
      case DioExceptionType.connectionError:
      case DioExceptionType.unknown:
        throw UnknownException(
          requestOptions: err.requestOptions,
        );
    }
    super.onError(err, handler);
  }
}

class UnauthorizedException extends DioException {
  UnauthorizedException({
    required RequestOptions requestOptions,
    Response? response,
  }) : super(
          requestOptions: requestOptions,
          response: response,
        );

  @override
  String toString() {
    return S.current.login;
  }
}

class TimeoutException extends DioException {
  TimeoutException({required super.requestOptions});

  @override
  String toString() {
    return S.current.login;
  }
}

class NoNetworkException extends DioException {
  NoNetworkException({required super.requestOptions});

  @override
  String toString() {
    return S.current.login;
  }
}

class UnknownException extends DioException {
  UnknownException({required super.requestOptions});

  @override
  String toString() {
    return S.current.login;
  }
}

class ServiceUnavailableException extends DioException {
  ServiceUnavailableException({required super.requestOptions});

  @override
  String toString() {
    return S.current.login;
  }
}
