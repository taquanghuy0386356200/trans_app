import 'package:flutter_boiler_2023/generated/l10n.dart';

abstract class Failure {
  final String? message;
  final int? code;

  const Failure({
    this.message,
    this.code,
  });
}

class UnknownFailure extends Failure {
  UnknownFailure({String? message})
      : super(message: message ?? S.current.unknown_error);
}

class ResponseFailure extends Failure {
  const ResponseFailure({required String message}) : super(message: message);
}
