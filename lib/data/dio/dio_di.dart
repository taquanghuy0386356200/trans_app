import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_boiler_2023/data/dio/interceptor/auth_interceptor.dart';
import 'package:flutter_boiler_2023/data/dio/interceptor/json_interceptor.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'interceptor/error_interceptor.dart';

abstract class DioDi {
  Dio get dio {
    final dio = Dio();
    dio.options.connectTimeout = const Duration(seconds: 30);
    dio.options.receiveTimeout = const Duration(seconds: 30);
    dio.interceptors.add(JsonResponseConverter());
    dio.interceptors.add(ErrorInterceptor());
    dio.interceptors.add(AuthInterceptor());

    if(kDebugMode) {
      dio.interceptors.add(dioLogger());
    }

    return dio;
  }

  PrettyDioLogger dioLogger() {
    return PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      maxWidth: 100,
    );
  }
}