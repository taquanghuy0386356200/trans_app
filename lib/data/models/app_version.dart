import 'package:json_annotation/json_annotation.dart';

part 'app_version.g.dart';

@JsonSerializable()
class AppVersion {
  const AppVersion({
    this.appCode,
    this.versionName,
    this.deviceType,
    this.version,
    this.invalid,
  });

  final String? appCode;
  final String? versionName;
  final String? deviceType;
  final int? version;

  /// Invalid: 1 - update required, 0 - update optional
  final int? invalid;

  factory AppVersion.fromJson(Map<String, dynamic> json) =>
      _$AppVersionFromJson(json);
}

@JsonSerializable()
class AppVersionResponse {
  final AppVersion? data;
  final MessageData? mess;

  const AppVersionResponse({this.data, this.mess});

  factory AppVersionResponse.fromJson(Map<String, dynamic> json) =>
      _$AppVersionResponseFromJson(json);
}

@JsonSerializable()
class MessageData {
  final String? description;
  final int? code;

  const MessageData({
    this.description,
    this.code,
  });

  factory MessageData.fromJson(Map<String, dynamic> json) =>
      _$MessageDataFromJson(json);
}
