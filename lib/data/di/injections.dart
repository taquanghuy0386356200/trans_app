import 'package:dio/dio.dart';
import 'package:flutter_boiler_2023/data/dio/dio_di.dart';
import 'package:flutter_boiler_2023/data/repository/crm_repository.dart';
import 'package:flutter_boiler_2023/data/services/services_example/crm_service.dart';
import 'package:flutter_boiler_2023/domain/flavors.dart';
import 'package:flutter_boiler_2023/presentation/global_bloc/global_bloc.dart';
import 'package:flutter_boiler_2023/presentation/home_tab/bloc/home_tab_bloc.dart';
import 'package:get_it/get_it.dart';

final GetIt getIt = GetIt.instance;

void configureDependencies() {
  ///Register Bloc
  getIt.registerLazySingleton<Dio>(() => _DioDi().dio);
  getIt.registerLazySingleton<GlobalBloc>(() => GlobalBloc());
  getIt.registerLazySingleton<HomeTabBloc>(() => HomeTabBloc());

  ///Register Services
  //Example
  getIt.registerLazySingleton<CrmServices>(
    () => CrmServices(
      getIt<Dio>(),
      baseUrl: F.crmURL,
    ),
  );

  ///Register Repository
  //Example
  getIt.registerLazySingleton<CrmRepository>(
    () => CrmRepositoryImpl(crmServices: getIt<CrmServices>()),
  );
}


class _DioDi extends DioDi {

}
