import 'package:dio/dio.dart';
import 'package:flutter_boiler_2023/data/models/app_version.dart';
import 'package:flutter_boiler_2023/data/dio/endpoint.dart';
import 'package:retrofit/retrofit.dart';

part 'crm_service.g.dart';

@RestApi()
abstract class CrmServices {
  factory CrmServices(Dio dio, {String baseUrl}) = _CrmServices;

  @GET(ApiConstant.GET_APP_VERSION)
  Future<AppVersionResponse> getAppVersion({
    @Query('appCode') String appCode = 'CPT',
    @Query('deviceType') String deviceType = 'ANDROID',
  });
}
