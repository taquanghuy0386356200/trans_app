import 'package:flutter_boiler_2023/data/dio/failure/failures.dart';
import 'package:flutter_boiler_2023/data/models/app_version.dart';
import 'package:flutter_boiler_2023/data/dio/network_constant.dart';
import 'package:flutter_boiler_2023/data/services/services_example/crm_service.dart';
import 'package:simple_result/simple_result.dart';

abstract class CrmRepository {
  Future<Result<AppVersion, Failure>> getAppVersion();
}

class CrmRepositoryImpl extends CrmRepository {
  final CrmServices _services;

  CrmRepositoryImpl({required CrmServices crmServices})
      : _services = crmServices;

  @override
  Future<Result<AppVersion, Failure>> getAppVersion() async {
    try {
      final response = await _services.getAppVersion();
      if (response.mess?.code != REQUEST_SUCCESS_CODE ||
          response.data == null) {
        return Result.failure(
          ResponseFailure(message: response.mess?.description ?? ''),
        );
      } else {
        return Result.success(response.data!);
      }
    } on Exception {
      return Result.failure(UnknownFailure());
    }
  }
}
