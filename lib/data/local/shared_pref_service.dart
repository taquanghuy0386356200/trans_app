import 'package:flutter_boiler_2023/constant/app_constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrefsService {
  static const _PREF_COUNT_REJECT_PERMISSION = '_PREF_COUNT_REJECT_PERMISSION';
  static const _PREF_IS_FIRST_INSTALL_APP = '_PREF_IS_FIRST_INSTALL_APP';
  static const _PREF_APP_VERSION = '_PREF_APP_VERSION';
  static const _PREF_CHECK_TIME_SHOW_REGIS_SMS =
      '_PREF_CHECK_TIME_SHOW_REGIS_SMS';

  static SharedPreferences? _preferencesInstance;

  static Future<SharedPreferences> get _instancePrefs async =>
      _preferencesInstance ??= await SharedPreferences.getInstance();

  static Future<SharedPreferences> init() async {
    _preferencesInstance = await _instancePrefs;
    return _preferencesInstance!;
  }

  static Future<bool> saveIsSkipOnBoardingPage(bool value) async {
    final prefs = await _instancePrefs;
    return prefs.setString(
      _PREF_IS_FIRST_INSTALL_APP,
      value.toString(),
    );
  }

  static Future<String> getIsSkipOnBoardingPage() async {
    final prefs = await _instancePrefs;
    final result =
        prefs.getString(_PREF_IS_FIRST_INSTALL_APP) ?? isFirstRun;
    return result;
  }
}
