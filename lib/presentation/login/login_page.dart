import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/widget/base_page.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      child: Container(
        color: AppTheme.getInstance().accentColor(),
        child: Text(S.current.login),
      ),
    );
  }
}
