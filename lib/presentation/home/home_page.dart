import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/home/bloc/home_bloc.dart';
import 'package:flutter_boiler_2023/presentation/widget/base_page.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';
import 'package:loader_overlay/loader_overlay.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc()..add(const GetExampleApi()),
      child: BlocListener<HomeBloc, HomeState>(
        listener: (context, state) {
          if (state is HomeLoading) {
            context.loaderOverlay.show();
          } else {
            context.loaderOverlay.hide();
          }
        },
        child: BaseScreen(
          child: ColoredBox(
            color: AppTheme.getInstance().accentColor(),
            child: Text(S.current.home),
          ),
        ),
      ),
    );
  }
}
