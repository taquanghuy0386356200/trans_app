import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_boiler_2023/data/di/injections.dart';
import 'package:flutter_boiler_2023/data/repository/crm_repository.dart';
import 'package:flutter_boiler_2023/data/services/services_example/crm_service.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:get_it/get_it.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<GetExampleApi>(_getFirstApi);
  }

  final _crmService = getIt.get<CrmRepository>();

  FutureOr<void> _getFirstApi(HomeEvent event, Emitter emit) async {
    emit(HomeLoading());

    final result = await _crmService.getAppVersion();
    result.when(
      success: (response) {
        emit(HomeSuccess());
      },
      failure: (failure) {
        emit(
          HomeError(
            message: failure.message ?? S.current.unknown_error,
          ),
        );
      },
    );
  }
}
