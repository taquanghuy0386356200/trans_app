import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/widget/base_page.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';

class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      child: ColoredBox(
        color: AppTheme.getInstance().accentColor(),
        child: Text(S.current.cart),
      ),
    );
  }
}
