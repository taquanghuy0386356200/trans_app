import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/widget/base_page.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      child: ColoredBox(
        color: AppTheme.getInstance().accentColor(),
        child: Text(S.current.account),
      ),
    );
  }
}
