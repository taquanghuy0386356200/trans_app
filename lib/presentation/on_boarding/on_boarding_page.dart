import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/data/local/shared_pref_service.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  State<OnBoardingPage> createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.getInstance().whiteColor(),
      body: SafeArea(
        child: Container(
          color: AppTheme.getInstance().bgColor(),
          child: GestureDetector(
            onTap: () async {
              await PrefsService.saveIsSkipOnBoardingPage(false);
            },
            child: Text(S.current.on_boarding),
          ),
        ),
      ),
    );
  }
}
