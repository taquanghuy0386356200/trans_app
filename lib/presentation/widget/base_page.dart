import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';

class BaseScreen extends StatelessWidget {
  const BaseScreen({
    Key? key,
    this.backGroundColor,
    required this.child,
    this.bottomNavigationBar,
  }) : super(key: key);

  final Color? backGroundColor;
  final Widget child;
  final Widget? bottomNavigationBar;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backGroundColor ?? AppTheme.getInstance().bgColor(),
      appBar: AppBar(),
      bottomNavigationBar: bottomNavigationBar,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SafeArea(
          child: Container(
            color: Colors.transparent,
            child: child,
          ),
        ),
      ),
    );
  }
}
