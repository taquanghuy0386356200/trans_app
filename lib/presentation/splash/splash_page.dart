import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boiler_2023/main.dart';
import 'package:flutter_boiler_2023/presentation/global_bloc/global_bloc.dart';
import 'package:flutter_boiler_2023/utils/routes/routes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  late GlobalBloc globalBloc;
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    globalBloc = context.read<GlobalBloc>();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1500),
    );
    _animation =
        CurvedAnimation(parent: _animationController, curve: Curves.easeIn);
    setupView();
  }

  Future<void> setupView() async {
    await Future.delayed(const Duration(seconds: 2), () {});
    globalBloc.add(const CheckIsFirstRunApp());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GlobalBloc, GlobalState>(
      listener: (BuildContext context, state) {
        if (state.isFirstRun) {
          navigatorKey.currentState?.pushNamed(Routes.onBoarding);
        } else {
          navigatorKey.currentState?.pushNamed(Routes.homeTab);
        }
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
          child: const Center(
            child: Text('Splash screen'),
          ),
        ),
      ),
    );
  }
}
