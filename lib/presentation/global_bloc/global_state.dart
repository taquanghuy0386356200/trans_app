part of 'global_bloc.dart';

class GlobalState extends Equatable {
  const GlobalState({
    this.isFirstRun = true,
  });
  final bool isFirstRun;

  GlobalState copyWith({
    bool? isFirstRun,
  }) {
    return GlobalState(
      isFirstRun: isFirstRun ?? this.isFirstRun,
    );
  }

  @override
  List<Object?> get props => [isFirstRun];
}
