import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boiler_2023/constant/app_constant.dart';
import 'package:flutter_boiler_2023/data/local/shared_pref_service.dart';

part 'global_event.dart';
part 'global_state.dart';

class GlobalBloc extends Bloc<GlobalEvent, GlobalState> {
  GlobalBloc() : super(const GlobalState()) {
    on<CheckIsFirstRunApp>(_checkIsFirstRunApp);
  }

  int x = 0;

  FutureOr<void> _checkIsFirstRunApp(event, emit) async {
    final isFirstRunLocal = await PrefsService.getIsSkipOnBoardingPage();
    if (isFirstRunLocal == isFirstRun) {
      x = 2;
      emit(
        state.copyWith(isFirstRun: true),
      );
    } else {
      x = 3;
      emit(
        state.copyWith(isFirstRun: false),
      );
    }
  }
}
