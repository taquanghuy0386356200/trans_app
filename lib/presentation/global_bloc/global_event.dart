part of 'global_bloc.dart';

abstract class GlobalEvent extends Equatable {
  const GlobalEvent();
}

class CheckIsFirstRunApp extends GlobalEvent {
  const CheckIsFirstRunApp();

  @override
  List<Object?> get props => [];
}
