import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/widget/base_page.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';
import 'package:flutter_boiler_2023/utils/extensions/text_style.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      child: ColoredBox(
        color: AppTheme.getInstance().accentColor(),
        child: Text(
          S.current.search,
          style: TextStyleApp.textStyle(fontSize: 14),
        ),
      ),
    );
  }
}
