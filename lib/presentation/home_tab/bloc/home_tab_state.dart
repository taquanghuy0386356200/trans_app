part of 'home_tab_bloc.dart';

abstract class HomeTabState extends Equatable {
  const HomeTabState();
}

class HomeTabInitial extends HomeTabState {
  @override
  List<Object> get props => [];
}
