import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/presentation/account/account_page.dart';
import 'package:flutter_boiler_2023/presentation/cart/cart_page.dart';
import 'package:flutter_boiler_2023/presentation/home/home_page.dart';
import 'package:flutter_boiler_2023/presentation/menu/menu_page.dart';
import 'package:flutter_boiler_2023/presentation/search/search_page.dart';

part 'home_tab_event.dart';
part 'home_tab_state.dart';

class HomeTabBloc extends Bloc<HomeTabEvent, HomeTabState> {
  HomeTabBloc() : super(HomeTabInitial()) {
    on<HomeTabEvent>((event, emit) {});
  }

  int currentIndex = 0;

  List<Widget> screens = [
    const MenuPage(),
    const SearchPage(),
    const HomePage(),
    const AccountPage(),
    const CartPage(),
  ];
}
