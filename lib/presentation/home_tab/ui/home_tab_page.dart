import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/data/di/injections.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/home_tab/bloc/home_tab_bloc.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';
import 'package:flutter_boiler_2023/utils/extensions/image_extension.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeTabsPage extends StatefulWidget {
  const HomeTabsPage({super.key});

  @override
  State<HomeTabsPage> createState() => _HomeTabsPageState();
}

class _HomeTabsPageState extends State<HomeTabsPage> {
  late int _currentIndex;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _currentIndex = 0;
    _pageController = PageController(initialPage: _currentIndex);
  }

  void _onPageChanged(int pageIndex) {}

  Color getColorBottomNavItem({required int index}) {
    if (_currentIndex == index) {
      return AppTheme.getInstance().actionIcon();
    } else {
      return AppTheme.getInstance().disableColor();
    }
  }

  void _onTabTapped(int tabIndex) {
    setState(() {
      _currentIndex = tabIndex;
    });
    _pageController.animateToPage(
      tabIndex,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }

  @override
  Widget build(BuildContext context) {
    final homeTabBloc = getIt<HomeTabBloc>();
    return Scaffold(
      body: PageView(
        controller: _pageController,
        onPageChanged: _onPageChanged,
        children: homeTabBloc.screens,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: _onTabTapped,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedItemColor: AppTheme.getInstance().actionIcon(),
        unselectedItemColor: AppTheme.getInstance().disableColor(),
        selectedIconTheme: IconThemeData(
          color: AppTheme.getInstance().actionIcon(),
        ),
        unselectedIconTheme: IconThemeData(
          color: AppTheme.getInstance().disableColor(),
        ),
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              ImageAssests.menuIcon,
              height: 20.h,
              width: 20.w,
              color: getColorBottomNavItem(index: 0),
            ),
            label: S.current.menu,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              ImageAssests.searchIcon,
              height: 20.h,
              width: 20.w,
              color: getColorBottomNavItem(index: 1),
            ),
            label: S.current.search,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              ImageAssests.homeIcon,
              height: 20.h,
              width: 20.w,
              color: getColorBottomNavItem(index: 2),
            ),
            label: S.current.home,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              ImageAssests.userIcon,
              height: 20.h,
              width: 20.w,
              color: getColorBottomNavItem(index: 3),
            ),
            label: S.current.account,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              ImageAssests.cartIcon,
              height: 20.h,
              width: 20.w,
              color: getColorBottomNavItem(index: 4),
            ),
            label: S.current.cart,
          ),
        ],
      ),
    );
  }
}
