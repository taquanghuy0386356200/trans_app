import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boiler_2023/data/di/injections.dart';
import 'package:flutter_boiler_2023/domain/flavors.dart';
import 'package:flutter_boiler_2023/generated/l10n.dart';
import 'package:flutter_boiler_2023/presentation/global_bloc/global_bloc.dart';
import 'package:flutter_boiler_2023/presentation/home_tab/bloc/home_tab_bloc.dart';
import 'package:flutter_boiler_2023/presentation/splash/splash_page.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';
import 'package:flutter_boiler_2023/resources/colors.dart';
import 'package:flutter_boiler_2023/utils/routes/routes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loader_overlay/loader_overlay.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

Future<void> startApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  configureDependencies();
  runApp(const App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  final String defaultLocale = Platform.localeName;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      minTextAdapt: true,
      builder: (_, __) {
        return FutureBuilder(
          future: getIt.allReady(),
          builder: (context, snapshot) {
            return GlobalLoaderOverlay(
              overlayColor: colorOverLay,
              child: MaterialApp(
                navigatorKey: navigatorKey,
                title: F.titleApp,
                debugShowCheckedModeBanner: false,
                supportedLocales: S.delegate.supportedLocales,
                theme: ThemeData(
                  primaryColor: AppTheme.getInstance().primaryColor(),
                  cardColor: Colors.white,
                  textTheme:
                      GoogleFonts.latoTextTheme(Theme.of(context).textTheme),
                  appBarTheme: const AppBarTheme(
                    color: Colors.white,
                    systemOverlayStyle: SystemUiOverlayStyle.dark,
                  ),
                  dividerColor: Colors.black,
                  scaffoldBackgroundColor: Colors.white,
                  textSelectionTheme: TextSelectionThemeData(
                    cursorColor: AppTheme.getInstance().primaryColor(),
                    selectionColor: AppTheme.getInstance().primaryColor(),
                    selectionHandleColor: AppTheme.getInstance().primaryColor(),
                  ),
                ),
                localizationsDelegates: const [
                  S.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                routes: Routes.routes,
                home: MultiBlocProvider(
                  providers: [
                    BlocProvider(create: (context) => getIt<GlobalBloc>()),
                    BlocProvider(create: (context) => getIt<HomeTabBloc>()),
                  ],
                  child: const SplashPage(),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
