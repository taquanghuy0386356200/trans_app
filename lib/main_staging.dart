import 'package:flutter_boiler_2023/domain/flavors.dart';
import 'package:flutter_boiler_2023/main.dart';

void main() {
  F.appFlavor = Flavor.stg;
  startApp();
}