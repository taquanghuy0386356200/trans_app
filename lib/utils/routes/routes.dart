import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/presentation/home/home_page.dart';
import 'package:flutter_boiler_2023/presentation/home_tab/ui/home_tab_page.dart';
import 'package:flutter_boiler_2023/presentation/login/login_page.dart';
import 'package:flutter_boiler_2023/presentation/on_boarding/on_boarding_page.dart';
import 'package:flutter_boiler_2023/presentation/splash/splash_page.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String login = '/login';
  static const String onBoarding = '/onBoarding';
  static const String home = '/home';
  static const String homeTab = '/homeTab';

  static final routes = <String, WidgetBuilder>{
    splash: (BuildContext context) => const SplashPage(),
    onBoarding: (BuildContext context) => const OnBoardingPage(),
    login: (BuildContext context) => const LoginPage(),
    homeTab: (BuildContext context) => const HomeTabsPage(),
    home: (BuildContext context) => const HomePage(),
  };
}
