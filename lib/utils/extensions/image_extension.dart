import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

String baseImage = 'assets/images';

class ImageAssests {
  static String headerChangeInfoClient =
      '$baseImage/header_change_info_client.png';
  static String homeIcon = '$baseImage/home.png';
  static String cartIcon = '$baseImage/cart.png';
  static String menuIcon = '$baseImage/menu.png';
  static String searchIcon = '$baseImage/search.png';
  static String userIcon = '$baseImage/user.png';

  static SvgPicture svgAssets(
    String name, {
    Color? color,
    double? width,
    double? height,
    BoxFit? fit,
    BlendMode? blendMode,
  }) {
    final w = width;
    final h = height;
    return SvgPicture.asset(
      name,
      colorBlendMode: blendMode ?? BlendMode.srcIn,
      color: color,
      width: w,
      height: h,
      fit: fit ?? BoxFit.none,
    );
  }
}
