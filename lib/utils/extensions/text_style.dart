import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/resources/app_theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class TextStyleApp {
  TextStyleApp._();

  static TextStyle textStyle({
    Color? color,
    double? fontSize,
    FontWeight? fontWeight,
  }) {
    return GoogleFonts.notoSans(
      color: color ?? AppTheme.getInstance().dfTxtColor(),
      fontSize: (fontSize ?? 14).sp,
      fontWeight: fontWeight ?? FontWeight.w400,
    );
  }
}
