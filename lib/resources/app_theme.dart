import 'package:flutter/material.dart';
import 'package:flutter_boiler_2023/resources/colors.dart';

enum AppMode {
  light,
  dark,
  customize
}

class AppTheme {
  static AppColor? _instance;

  static AppColor getInstance() {
    ///todo chỗ này đối với trường hợp theo device thì ok, còn sót trường hợp lưu theo app
    _instance ??= (WidgetsBinding.instance.platformDispatcher.platformBrightness == Brightness.light) ? LightApp() : DarkApp();
    return _instance!;
  }

}
