enum Flavor {
  dev,
  stg,
  prod,
}

class F {
  static Flavor? appFlavor;

  static String get titleApp {
    switch (appFlavor) {
      case Flavor.dev:
        return 'Boiler (develop)';
      case Flavor.stg:
        return 'Boiler (staging)';
      case Flavor.prod:
        return 'Boiler (product)';
      default:
        return 'Boiler (staging)';
    }
  }

  static String get envUrl {
    switch (appFlavor) {
      case Flavor.dev:
        return 'dev';
      case Flavor.stg:
        return 'stg';
      case Flavor.prod:
        return 'prod';
      default:
        return 'stg';
    }
  }

  static String get crmURL {
    switch (appFlavor) {
      case Flavor.dev:
        return 'http://10.248.158.7:9789';
      case Flavor.stg:
        return 'https://backendtest.epass-vdtc.com.vn/crm2';
      case Flavor.prod:
        return 'https://backend.epass-vdtc.com.vn/crm2';
      default:
        return 'crmURL';
    }
  }
}
